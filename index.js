// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoute");
// This allow us to use all the routes defined in taskRoute.js

// Server Setup
const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({extended:true}));
//.use para magamit ang pinaka-base url

// Database connection
mongoose.connect( 												// Para maiconnect ang mongodb atlas sa server 
	"mongodb+srv://admin:admin@zuitt-bootcamp.z9use.mongodb.net/batch127_to-do?retryWrites=true&w=majority",
		{														// Replace <password> and database name
			useNewUrlParser: true, 	// to avoid any current or future errors
			useUnifiedTopology: true // to prevent current or futureerrors
	});

// Error handling
let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database"))

//Add the task route
app.use("/tasks", taskRoute);
// It allows all the task routes created in the taskRoute.js file to use "/tasks" route
//localhost:3001/tasks

app.listen(port, () => console.log(`Now listening to port ${port}`));

// Model =>(export to) Controllers =>(export to) Routes =>(export to) index.js(main server)