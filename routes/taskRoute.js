
// Start of Activity at Line 57

// Routescontains ALL the endpoint for our application
// Then we separate the routes such that index.js only contains information on the server
// We need to use express.Router() function to achieve this
const express = require("express");
const router = express.Router();
// Creates a Router instance that functions as a middleware and routing system
// Allows access to HTTP method middlewares that makes it easier to create routes for our application

const taskController = require("../controllers/taskController")
// the "taskController" allows us to use the functions defined in the taskController.js file
// imported folder

//localhost:3000/tasks/getAll
router.get("/getAll", (req, res) => {
	// Invokes the get all tasks function from the "taskController.js" file and sends the result back to the client
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
	// "resultFromController" is only used here to make the code easier to understand, but it is common
		// practice to use the shorthand parameter name for a result using the parameter name "result" / "res"
})
// Routes are responsible for defining the URI's that our client accesses and the corresponding controller
// functions that will be used when a route is accessed.

// Create new task
router.post("/", (req, res) =>{
	taskController.createTask(req.body).then(result => res.send(result));
	// The createTask function needs the data from the request body, so we need to supply it to thee function
})	// If information will be coming from the client-side commonly from form, the data can be accessed from the request "body" property

// Model =>(export to) Controllers =>(export to) Routes =>(export to) index.js(main server)


// To Delete a Task
// This route expects to receive a DELETE request at the URL ""
// http://localhost:3001/tasks/:id
router.delete('/:id', (req, res) =>{
	taskController.deleteTask(req.params.id).then(result => res.send(result));
	// If the information will be coming from the URL, the data can be accessed from the request "params" property
	// In this case, "id" is the parameter, the property name of this object will match the given URL parameter
	// req.params.id
})
// the task ID is obtained from the URL is denoted by the ":id" identifier in the route
// The colon ( : ) is an identifyer that helps create a dynamic route which allows us to supply information in the URL
/// The word that comes after the colon symbol will be the name of the URL parameter
// ":id" is a WILDCARD where you can put any value, it then creates a link between "id" parameter in the URL and the value provided in the URL
	// eg
		// if route is localhost:3000/tasks/1234
		// 1234 is assigned to the "id" parameter in the URL


// Update a task
router.put("/:id", (req, res) =>{
	taskController.updateTask(req.params.id, req.body).then(result => res.send(result));
	// req.params.id retrieves the taskId from the parameter
	// req.body reteieved the data of the updates that will be applied to a task from the request body
})

// Start of Activity
router.get("/:id", (req, res) => {
	taskController.getOneTask(req.params.id)
	.then(specificTask => {
		res.send(specificTask);
	})
})

router.put("/:id/complete", (req, res) =>{
	taskController.updatedTask(req.params.id, req.body)
	.then(result =>{
		res.send(result)
	})
})


module.exports = router;
// para maexport ang buong file


// Pwede iconnect sa index.js