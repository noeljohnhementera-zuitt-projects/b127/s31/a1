
// Start of Activity at Line 108

// Controllers contain the functions and business logic of our express application
// Meaning, all the operation can be placed in this file eg, error handling

const Task = require("../models/task");
// Allows us to use the content of the "task.js" file in the models folder
// this worked because of the require

// Controller function for getting all the tasks

module.exports.getAllTasks = () => {		// connected sa taskRoute.js
	return Task.find({}).then(result => {
		return result;
		// The return here, returns the result of the MongoDB query to the "result" parameter defined
			// in the "then" method
	})
}
	// The return statement returns the resule of the mongoose method "find" back to the 
		// "taskRoute.js" file which invokes this function when the "/tasks" routes is accessed
	// The ".then" method is used to wait for the Mongoose "find" method tpo finish before sending the 
		// result back to the route and eventually to the client	

	//taskController.js > taskRoute.js


// Controller function for creating task

// The request body coming from the client was passed from the "taskRoute.js" file via the req.body
	//as an argument and is renamed "requestBody" parameter in the controller file
module.exports.createTask = (requestBody) => {
	// Create a task object based on the Mongoose model Task
	let newTask = new Task({
		name: requestBody.name
	})
	// Saves the newly created "newTask" object in the MongoDB database
	// .then method waits until the task is stored/error
	// .then method will accept 2 arguments:
		// first parameter will store the result returned by the save method
		// second parameter will store the error object
	// Compared to using a callback function on Mongoose methods discussed in the previous session, the first parameter stores the error and the result is stored in the second parameter
	// if .then, mauna dapat ang result(task) instead na error
		return newTask.save().then((task, error)=>{
			// if the error is encountered, the "return" statement will prevent any other line or code below it and within the same code block from executing
			// Since the following return statement is nested within the "then" method chained to the "save" method, they do not prevent each other from executing code
			// kasama ni .then si return
			if(error){
				console.log(error);
				return false;
			}else{
				return task;
			}
	})
}


// Delete Task
/*
Business Logic
1. Look for the task with the corresponding id provided in the URL /route
2. Delete the task using the Mongoose method "findByIdAndRemove" with the same id provided in the route
*/

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err){
			// if an error is encountered returns a "false" boolean back to the client
			console.log(err)
			return false;
		}else{
			return removedTask;
			// Delete successful, returns the removed task object back to the client
		}
	})
}

// Updating a task
/*
Business logic
1  Get the task with the id using the method "findById"
2. Replace the task's name returned from the database with the "name" property from the request body
3. Save the task
*/

module.exports.updateTask = (taskId, newContent) =>{
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error)
			return false;
		}
		result.name = newContent.name;
		return result.save().then((updatedTask, saveErr)=>{
			if(saveErr){
				console.log(saveErr)
				return false;
			}else{
				return updatedTask
				// Update successfully, returns the updated task object back to the client
			}
		})
			// results of the "findById" method will be stored in the "result" parameter
			// Its "name" property will be reassigned the value of the "name" received from the request body
		
	})
}

// Start of Activity
module.exports.getOneTask = () => {
	return Task.findOne()
	.then(result => {
		return result;
	})
}

module.exports.updatedTask = (taskId, newStatus) => {
	return Task.findById(taskId)
	.then((result, error) =>{
		if(error){
			return false
		}else{
			result.status = newStatus.status
			return result.save()
			.then((updatedStatus, saveErr) =>{
				if(saveErr){
					return false
				}else{
					return updatedStatus
				}
			})
		}
	})
}